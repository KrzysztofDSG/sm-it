working_directory "/var/www/rails/deploy/sm-it"
pid "/var/www/rails/deploy/sm-it/tmp/pids/unicorn.pid"
stderr_path "/var/www/rails/deploy/sm-it/log/unicorn.log"
stdout_path "/var/www/rails/deploy/sm-it/log/unicorn.log"

listen "/tmp/unicorn.sm-it.sock"
worker_processes 1
timeout 30 


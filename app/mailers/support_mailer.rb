class SupportMailer < ApplicationMailer

	def support(name, phone, email, question)
		
		@name_web, @phone_web, @email_web, @question_web = name, phone, email, question

		delivery_options = { user_name: "info@sm-it.pl",
		       password: "barabol",
		       address: "sm-it.pl",
		       domain: "sm-it.pl",
		       authentication: "plain",
		       enable_starttls_auto: true,
		       openssl_verify_mode: 'none',
		       port: 587 }

		@rec_email = "info@sm-it.pl"

		mail(to: "#{@rec_email}",
			reply_to: "#{@name} <#{@email}>",
			from: "Info <info@sm-it.pl>",
			subject: "Wiadomość z formularza kontaktowego strony www",
			delivery_method_options: delivery_options)
		
	end


end
class HomeController < ApplicationController

  def index
    
  end

  def contact_web 
    @name_web = params[:contact_web][:name_web]
    @phone_web = params[:contact_web][:phone_web]
    @email_web = params[:contact_web][:email_web]
    @question_web = params[:contact_web][:client_message]
          
    if @question_web !="" 
		SupportMailer.support(@name_web, @phone_web, @email_web, @question_web).deliver_later
		flash[:alert_success] = 'Dziękujemy za Twoje zapytanie. Skontaktujemy z Tobą możliwie szybko!.' 
	else
		flash[:alert_warning] = 'UWAGA! Uzupełnij pole wiadomośći.' 
	end
    
    redirect_to root_path
  end

end